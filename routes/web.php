<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('register', 'AuthController@registerView');
Route::post('register', 'AuthController@register');

Route::get('login', 'AuthController@loginView')->name('login');
Route::post('login', 'AuthController@login');

Route::post('check_email_availability', 'AuthController@checkEmailAvailability');
Route::post('check_username_availability', 'AuthController@checkUsernameAvailability');
Route::post('match_passwords', 'AuthController@matchPasswords');

Route::group(['middleware' => ['auth']], function () {
    
//    if the user is not subscribed to a plan only then he/she can access these routes
    Route::group(['middleware' => ['not_subscribed']], function () {
        
//        subscription page
        Route::get('subscription', 'UserController@subscription');
        
        Route::post('subscribe', 'UserController@subscribe');
        
    });
    
//    if the user is subscribed to a plan only then he/she can access these routes    
    Route::group(['middleware' => ['subscribed']], function () {

        Route::get('/', 'UserController@dashboard');
        Route::get('dashboard', 'UserController@dashboard');
        
        Route::get('cancel_subscription', 'UserController@cancelSubscription');

        Route::get('add_post', 'UserController@addPostView');
        Route::post('add_post', 'UserController@addPost');

        Route::get('logout', 'UserController@logout');
        
    });
    
});
