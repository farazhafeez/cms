<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotSubscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = \Illuminate\Support\Facades\Auth::user();
        if ($user->getSubscription) {
            $sub = \Stripe\Subscription::retrieve($user->getSubscription->stripe_id);
            if($sub->status == 'active') {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
