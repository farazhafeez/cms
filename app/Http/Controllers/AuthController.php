<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthController extends Controller {

    function registerView() {
        if (Auth::check()) {
            return redirect('dashboard');
        } else {
            return view('register');
        }
    }

    function register(Request $request) {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required | unique:users',
            'email' => 'required | email | unique:users',
            'password' => 'required | min:6 | confirmed',
            'password_confirmation' => 'required'
        ]);
        User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        Session::flash('success', 'Account created successfully.');
        return Redirect::to(URL::previous());
    }

    function loginView() {
        if (Auth::check()) {
            return redirect('dashboard');
        } else {
            return view('login');
        }
    }

    function login(Request $request) {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        if (Auth::attempt(['username' => $request['username'], 'password' => $request['password']])) {
            return redirect('dashboard');
        } else if (Auth::attempt(['email' => $request['username'], 'password' => $request['password']])) {
            return redirect('dashboard');
        } else {
            Session::flash('error', 'Invalid Email or Password');
            return Redirect::to(URL::previous())->withInput();
        }
    }

    function checkUsernameAvailability(Request $request) {
        if (User::where('username', $request['username'])->first()) {
            return response()->json(false);
        }
        return response()->json(true);
    }

    function checkEmailAvailability(Request $request) {
        if (User::where('email', $request['email'])->first()) {
            return response()->json(false);
        }
        return response()->json(true);
    }

    function matchPasswords(Request $request) {
        if ($request['password'] == $request['password_confirmation']) {
            return response()->json(true);
        }
        return response()->json(false);
    }

}
