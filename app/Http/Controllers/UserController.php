<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Post;
use App\PostImage;

class UserController extends Controller {

    private $userId;
    private $user;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->userId = Auth::user()->id;
            $this->user = Auth::user();
            return $next($request);
        });
    }

    function dashboard() {
        $data['is_on_grace_period'] = 0;
        if ($this->user->subscription('Monthly')->onGracePeriod()) {
           $data['is_on_grace_period'] = 1;
        }
        $data['posts'] = Post::with('images', 'user')->orderBy('created_at', 'desc')->get();
        return view('dashboard', $data);
    }

    function subscription() {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = $this->user;
        if ($user->getSubscription) {
            $sub = \Stripe\Subscription::retrieve($user->getSubscription->stripe_id);
            if($sub->status == 'active') {
                return redirect('/');
            }
        }
        return view('subscription');
    }

    function subscribe(Request $request) {
        $token = $request['stripeToken'];
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        try {
            $user = User::find($this->userId);
            $user->newSubscription('Monthly', 'plan_F9syTx27apIkdP')->create($token);
            return Redirect::to(URL::previous());
        } catch (\Stripe\Error\Card $e) {
            Session::flash('error', $e->getMessage());
            return Redirect::to(URL::previous());
        } catch (\Stripe\Error\RateLimit $e) {
            Session::flash('error', $e->getMessage());
            return Redirect::to(URL::previous());
        } catch (\Stripe\Error\InvalidRequest $e) {
            Session::flash('error', $e->getMessage());
            return Redirect::to(URL::previous());
        } catch (\Stripe\Error\Authentication $e) {
            Session::flash('error', $e->getMessage());
            return Redirect::to(URL::previous());
        } catch (\Stripe\Error\ApiConnection $e) {
            Session::flash('error', $e->getMessage());
            return Redirect::to(URL::previous());
        } catch (\Stripe\Error\Base $e) {
            Session::flash('error', $e->getMessage());
            return Redirect::to(URL::previous());
        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
            return Redirect::to(URL::previous());
        }
    }

    function cancelSubscription() {
        $user = User::find($this->userId);
        $user->subscription('Monthly')->cancel();
        return Redirect::to(URL::previous());
    }

    function addPostView() {
        return view('add_post');
    }

    function addPost(Request $request) {
        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        $post = Post::create([
                    'title' => $request['title'],
                    'description' => $request['description'],
                    'user_id' => $this->userId
        ]);
        if (isset($request['images'])) {
            foreach ($request['images'] as $image) {
                $imageName = str_random(10) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('../public/images/');
                PostImage::create([
                    'file' => 'public/images/' . $imageName,
                    'post_id' => $post->id,
                ]);
                $image->move($destinationPath, $imageName);
            }
        }
        Session::flash('success', 'Post added successfully.');
        return Redirect::to(URL::previous());
    }

    function logout() {
        Auth::logout();
        return Redirect::to(URL::previous());
    }

}
