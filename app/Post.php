<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'user_id'
    ];
    
    function images(){
        return $this->hasMany(PostImage::class);
    }
    
    function user(){
        return $this->belongsTo(User::class);
    }
}
