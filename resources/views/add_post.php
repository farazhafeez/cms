<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Add Post</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= asset('/') ?>">CMS</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="<?= asset('add_post') ?>">Add Post</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?= asset('logout') ?>">Logout</a></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Add Post</h3>
                    <?php
                    if ($errors->any()) {
                        foreach ($errors->all() as $error) {
                            ?>
                            <h6 class="alert alert-danger"> <?php echo $error ?></h6>
                            <?php
                        }
                    }
                    if (Session::has('error')) {
                        ?>
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Session::get('error') ?>
                        </div>
                    <?php } if (Session::has('success')) {
                        ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Session::get('success') ?>
                        </div>
                    <?php } ?>
                    <form action="<?= asset('add_post') ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                        <div class="form-group has-feedback">
                            <input name="title" type="text" class="form-control" placeholder="Title" >
                        </div>
                        <div class="form-group has-feedback">
                            <textarea name="description" class="form-control" placeholder="Description"></textarea>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="file" name="images[]" multiple accept="image/*">
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>
</html>