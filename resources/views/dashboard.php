<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Dashboard</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= asset('/') ?>">CMS</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="<?= asset('add_post') ?>">Add Post</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <?php if ($is_on_grace_period == 1) { ?>
                            <a href="#">You have successfully canceled you subscription and now you are on grace period.</a>
                        <?php } else { ?>
                            <a href="<?= asset('cancel_subscription') ?>">Cancel Subscription</a>
                        <?php } ?>
                    </li>
                    <li><a href="<?= asset('logout') ?>">Logout</a></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <h3>Blog</h3>
            <?php foreach ($posts as $post) { ?>
                <div class="container" style="border: 1px solid grey; margin-top: 25px; margin-bottom: 25px">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= $post->title ?></h3>
                            <p><?= $post->description ?></p>
                            <p>Posted by: <?= $post->user->first_name . ' ' . $post->user->last_name ?></p>
                            <?php foreach ($post->images as $image) { ?>
                                <img src="<?= asset($image->file) ?>" height="320" width="480">
                                    <!--<div style="background-image:url('<?= asset($image->file) ?>');background-size: cover;background-position: center center;"></div>-->
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </body>
</html>