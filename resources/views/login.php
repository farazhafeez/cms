<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="<?= csrf_token() ?>">
        <title>Login</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <style>
            label.error, p.error {
                color:red;
            }
        </style>
    </head>
    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="register-box-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <h3 class="login-box-msg"><strong>Login</strong></h3>
                            <?php
                            if ($errors->any()) {
                                foreach ($errors->all() as $error) {
                                    ?>
                                    <h6 class="alert alert-danger"> <?php echo $error ?></h6>
                                    <?php
                                }
                            }
                            if (Session::has('error')) {
                                ?>
                                <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                                    <?php echo Session::get('error') ?>
                                </div>
                            <?php } if (Session::has('success')) {
                                ?>
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                                    <?php echo Session::get('success') ?>
                                </div>
                            <?php } ?>
                            <form action="<?= asset('login') ?>" method="post">
                                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                                <div class="form-group has-feedback">
                                    <input name="username" type="text" class="form-control" placeholder="Username" >
                                    <p class="error" id="verify-username"></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <input name="password" type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                                    </div>
                                </div>
                            </form>
                            <div class="login_ftr">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <a href="<?= asset('register') ?>" class="text-center">Doesn't have an account<strong> Register?</strong></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    </body>
</html>
