<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Subscription</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <?php
                    if ($errors->any()) {
                        foreach ($errors->all() as $error) {
                            ?>
                            <h6 class="alert alert-danger"> <?php echo $error ?></h6>
                            <?php
                        }
                    }
                    if (Session::has('error')) {
                        ?>
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Session::get('error') ?>
                        </div>
                    <?php } if (Session::has('success')) {
                        ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Session::get('success') ?>
                        </div>
                    <?php } ?>
                    <h5>You have to pay $10 per month to use this system</h5>
                    <form action="<?= asset('subscribe') ?>" method="POST">
                        <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                        <input type="hidden" name="plan_type" value="2">
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="pk_test_C4rn73a1qaKs7n5QSyAjflJC002vNkHZCe"
                            data-amount="1000"
                            data-name="CMS Montly"
                            data-description="Monthly $10"
                            data-label="$10/monthly"
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-locale="auto">
                        </script>
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>

    </body>
</html>