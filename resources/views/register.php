<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="<?= csrf_token() ?>">
        <title>Register</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <style>
            label.error, p.error {
                color:red;
            }
        </style>
    </head>
    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="register-box-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <h3 class="login-box-msg"><strong>Sign up</strong></h3>
                            <?php
                            if ($errors->any()) {
                                foreach ($errors->all() as $error) {
                                    ?>
                                    <h6 class="alert alert-danger"> <?php echo $error ?></h6>
                                    <?php
                                }
                            }
                            if (Session::has('error')) {
                                ?>
                                <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                                    <?php echo Session::get('error') ?>
                                </div>
                            <?php } if (Session::has('success')) {
                                ?>
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                                    <?php echo Session::get('success') ?>
                                </div>
                            <?php } ?>
                            <form id="user-register" action="<?= asset('register') ?>" method="post">
                                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                                <div class="form-group has-feedback">
                                    <input name="first_name" type="text" class="form-control" placeholder="First name" value="<?php echo old('first_name'); ?>">
                                </div>
                                <div class="form-group has-feedback">
                                    <input name="last_name" type="text" class="form-control" placeholder="Last name" value="<?php echo old('last_name'); ?>">
                                </div>
                                <div class="form-group has-feedback">
                                    <input name="username" onkeyup="verifyUsername()" type="text" class="form-control" placeholder="Username" value="<?php echo old('username'); ?>">
                                    <p class="error" id="verify-username"></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <input name="email" onkeyup="verifyEmail()"  type="email" class="form-control" placeholder="Email" value="<?php echo old('email'); ?>">
                                    <p class="error" id="verify-email"></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <input name="password" type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group has-feedback">
                                    <input name="password_confirmation" onkeyup="verifyPassword()" type="password" class="form-control" placeholder="Confirm password">
                                    <p class="error" id="verify-password"></p>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign up</button>
                                    </div>
                                </div>
                            </form>
                            <div class="login_ftr">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <a href="<?= asset('login') ?>" class="text-center">Already have an account<strong> Login?</strong></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
            <!-- /.form-box -->
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script>
                                        $("#user-register").validate({
                                            rules: {
                                                email: {
                                                    required: true,
                                                    email: true,
                                                },
                                                username: {
                                                    required: true
                                                },
                                                password: {
                                                    required: true,
                                                    minlength: 6
                                                },
                                                password_confirmation: {
                                                    required: true
                                                },
                                                first_name: {
                                                    required: true
                                                },
                                                last_name: {
                                                    required: true
                                                }
                                            }
                                        });

                                        function verifyEmail() {
                                            var email = $('input[name=email]').val();
                                            let result = false;
                                            $.ajax({
                                                type: "POST",
                                                data: {"email": email},
                                                url: "<?= asset('check_email_availability') ?>",
                                                async: false,
                                                beforeSend: function (request) {
                                                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                                },
                                                success: function (data) {
                                                    if (data) {
                                                        $('#verify-email').html('');
                                                        result = data;
                                                    } else {
                                                        $('#verify-email').html('Email already in use.');
                                                        result = data;
                                                    }
                                                }
                                            });
                                            return result;
                                        }

                                        function verifyUsername() {
                                            var username = $('input[name=username]').val();
                                            let result = false;
                                            $.ajax({
                                                type: "POST",
                                                data: {"username": username},
                                                url: "<?= asset('check_username_availability') ?>",
                                                async: false,
                                                beforeSend: function (request) {
                                                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                                },
                                                success: function (data) {
                                                    if (data) {
                                                        $('#verify-username').html('');
                                                        result = data;
                                                    } else {
                                                        $('#verify-username').html('Username is not available.');
                                                        result = data;
                                                    }
                                                }
                                            });
                                            return result;
                                        }

                                        function verifyPassword() {
                                            let password = $('input[name=password]').val();
                                            let password_confirmation = $('input[name=password_confirmation]').val();
                                            let result = false;
                                            $.ajax({
                                                type: "POST",
                                                data: {"password": password, 'password_confirmation': password_confirmation},
                                                url: "<?= asset('match_passwords') ?>",
                                                async: false,
                                                beforeSend: function (request) {
                                                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                                },
                                                success: function (data) {
                                                    if (data) {
                                                        $('#verify-password').html('');
                                                        result = data;
                                                    } else {
                                                        $('#verify-password').html('Password does not match.');
                                                        result = data;
                                                    }
                                                }
                                            });
                                            return result;
                                        }

                                        $('form').submit(function () {
                                            if (verifyUsername() && verifyPassword() && verifyEmail()) {
                                                return true;
                                            }
                                            return false;
                                        });
        </script>
    </body>
</html>
